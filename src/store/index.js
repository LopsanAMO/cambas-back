import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/users/auth';
import info from './modules/users/info';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    info,
  },
});
