import axios from '@/api';

const getters = {
  isAuthenticated: () => !!localStorage.getItem('token'),
  token: state => state.token,
};

const actions = {
  login({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post('users/login/', data)
        .then((res) => {
          console.log(res);
          localStorage.setItem('token', res.data.token);
          commit('authUser', res.data);
          resolve(res.data);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },
  signup({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post('users/', data)
        .then((res) => {
          console.log(res);
          localStorage.setItem('token', res.data.auth_token);
          commit('singUser', res.data);
          resolve(res.data);
        })
        .catch((error) => {
          reject(error.response.data);
        });
    });
  },
  facebook({ commit }, authData) {
    return new Promise((resolve, reject) => {
      axios.post('/facebook/', {
        backend: authData.backend,
        access_token: authData.access_token,
      })
        .then((res) => {
          commit('authUser', {
            idToken: res.data.token,
          });
          localStorage.setItem('token', res.data.token);
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  authUser(state, data) {
    this.state.token = data.token;
  },
  singUser(state, data) {
    this.state.token = data.auth_token;
  },
};

const state = {
  token: null,
  isAuthenticated: null,
  errors: null,
};

export default {
  state,
  getters,
  actions,
  mutations,
};
