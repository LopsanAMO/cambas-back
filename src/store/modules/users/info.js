import axios from '@/api';

const getters = {
  user: state => state.user,
  fbAID: state => state.fbId,
};

const actions = {
  getUserData({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('users/user/1/', { headers: { Authorization: `Token ${localStorage.getItem('token')}` } })
        .then((res) => {
          commit('setUserInfo', res.data);
          console.log(res.data);
          resolve(res.data);
        })
        .catch((err) => {
          console.log(err.response.data);
          reject(err.response.data);
        });
    });
  },
  updateUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.patch('users/user/9999/', data, { headers: { Authorization: `Token ${localStorage.getItem('token')}` } })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  },
  getFacebookPhoto({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get(`https://graph.facebook.com/${this.state.fbId}?fields=picture&type=large&access_token=${this.state.fbToken}`)
        .then((res) => {
          console.log(res);
          resolve(res.data);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },
  getFacebookCoverPhoto({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get(`https://graph.facebook.com/${this.state.fbId}?fields=cover&access_token=${this.state.fbToken}`)
        .then((res) => {
          console.log(res);
          resolve(res.data);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },
};

const mutations = {
  setUserInfo(state, data) {
    this.state.user = data;
    this.state.fbId = data.f_id;
    this.state.fbToken = data.f_token;
  },
};

const state = {
  user: '',
  fbId: '',
  fbToken: '',
};

export default {
  state,
  getters,
  actions,
  mutations,
};
