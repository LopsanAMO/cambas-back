import Vue from 'vue';
import FBSignInButton from 'vue-facebook-signin-button';
import VueMaterial from 'vue-material';
import Vuelidate from 'vuelidate';
import App from './App.vue';
import router from './router/index';
import store from './store/index';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

Vue.use(FBSignInButton);
Vue.use(VueMaterial);
Vue.use(Vuelidate);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    window.fbAsyncInit = () => {
      window.FB.init({
        appId: '572256429797009',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v2.12',
      });
      window.FB.AppEvents.logPageView();
    };

    (((d, s, id) => {
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      const js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk'));
  },
}).$mount('#app');
