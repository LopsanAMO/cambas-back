import Vue from 'vue';
import Router from 'vue-router';
import Index from '../views/Index.vue';
import Signup from '../views/users/Signup.vue';
import User from '../views/users/User.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index,
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/user',
      name: 'user',
      component: User,
    },
  ],
});
