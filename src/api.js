import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://cambas-back.herokuapp.com/api/v1/',
});

export default instance;
